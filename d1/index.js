
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	}
});

//INSERT MANY
/*
	-Syntax:
		-db.collectionName.insertMany([ {objectA}, {objectB} ])
*/

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	},
	course: ["Python", "React", "PHP"],
	department: "none"
},

{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
	},
	course: ["React", "Laravel", "Sass"]
}

])

//DELETE: DELETING DOCUMENTS

// For our example, let us create a document that we will delete

db.users.insert({
	firstName: "test"
});

// DELETE ONE: Deleting a single document 

/*
- Syntax:
	-db.collectionName.deleteOne({criteria})

*/

db.users.deleteOne({
	firstName: "test"
});

db.users.find({ firstName: "test"}).pretty();

// DELETE MANY: Delete many documents
/*
-Syntax:
	- db.collectionName.deleteMany({criteria});
*/

db.users.deleteMany({
	firstName: "Bill"
})